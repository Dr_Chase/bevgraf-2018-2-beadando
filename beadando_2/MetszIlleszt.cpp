#include <GL/glut.h>					// (or others, depending on the system in use)

typedef struct point2d { GLdouble x, y; } POINT2D;

POINT2D initPoint2D(GLdouble x, GLdouble y) {
    POINT2D P;
    P.x = x;
    P.y = y;
    return P;
}

typedef struct hom2d { GLdouble x, y, z; } HOM2D;

HOM2D initHom2D(GLdouble x, GLdouble y, GLdouble z) {
    HOM2D H;
    H.x = x;
    H.y = y;
    H.z = z;
    return H;
}

HOM2D convertIH(POINT2D P){
    HOM2D H;
    H.x = P.x;
    H.y = P.y;
    H.z = 1;
    return H;
}

POINT2D convertH(HOM2D H){
    POINT2D P;
   // if(H.z = 0){ return NULL;}
    P.x = H.x/H.z;
    P.y = H.y/H.z;
    return P;
}

HOM2D metszIlleszt(HOM2D a, HOM2D b){
    HOM2D H;
    H.x = a.y*b.z-a.z*b.y;
    H.y = -1*(a.x*b.z-a.z*b.x);
    H.z = a.x*b.y-a.y*b.x;
    return H;
}

//POINT2D points[4] = {initPoint2D(100, 40),initPoint2D(300, 200),initPoint2D(40, 200),initPoint2D(150,20)};
POINT2D points[4] = { initPoint2D(100, 40),initPoint2D(300, 40),initPoint2D(150, 200),initPoint2D(150,20) };

void init (void)
{
    glClearColor (1.0, 1.0, 1.0, 0.0);	// Set display-window color to white.

    glMatrixMode (GL_PROJECTION);		// Set projection parameters.
    gluOrtho2D (0.0, 400.0, 0.0, 300.0);
    glPointSize( 10.0 );
}

POINT2D metszesPont(POINT2D a,POINT2D b,POINT2D c,POINT2D d){
    HOM2D hom[4] = {convertIH(a), convertIH(b), convertIH(c), convertIH(d)};
    return convertH(metszIlleszt(metszIlleszt(hom[0],hom[1]),metszIlleszt(hom[2],hom[3])));
}

void lineSegment (void)
{    
    glBegin (GL_LINES);
        glVertex2i (points[0].x, points[0].y);       // Specify line-segment geometry.
        glVertex2i (points[1].x, points[1].y);
        glVertex2i (points[2].x, points[2].y);       // Specify line-segment geometry.
        glVertex2i (points[3].x, points[3].y);
    glEnd ( );
    
}

void point (void) {
     POINT2D metszespont = metszesPont(points[0],points[1],points[2],points[3]);
    glBegin( GL_POINTS );        
        glVertex2f( metszespont.x, metszespont.y );
    glEnd( );
}

void draw()
{
    glClear (GL_COLOR_BUFFER_BIT);

    glColor3f (0.0f, 0.4f, 0.2f);
    
    lineSegment();

    glColor3f (0.7f, 0.2f, 0.8f);

    point();

    glFlush ( );
}

int main (int argc, char** argv)
{
    glutInit (&argc, argv);                         // Initialize GLUT.
    glutInitDisplayMode (GLUT_SINGLE | GLUT_RGB);   // Set display mode.
    glutInitWindowPosition (50, 100);   // Set top-left display-window position.
    glutInitWindowSize (400, 300);      // Set display-window width and height.
    glutCreateWindow ("An Example OpenGL Program"); // Create display window.

    init ( );                            // Execute initialization procedure.
    glutDisplayFunc (draw);       // Send graphics to display window.
    glutMainLoop ( );                    // Display everything and wait.
    return 0;
}