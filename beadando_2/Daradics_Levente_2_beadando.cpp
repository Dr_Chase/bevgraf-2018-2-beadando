#include <GL/glut.h>
#include <bevgrafmath2017.h>
#include <cmath>
#include <vector>
#include <iostream>
#include <tuple>
#include <algorithm>

GLsizei winWidth = 800, winHeight = 600;
GLsizei winZero = 0;

GLint dragged = -1;
GLint pointSensitivity = 8;

float teglalapCsucs1x = 100.0f;
float teglalapCsucs1y = 100.0f;
vec2 teglalapCsucs1_vec2(teglalapCsucs1x, teglalapCsucs1y);

float teglalapCsucs2x = 600.0f;
float teglalapCsucs2y = 500.0f;
vec2 teglalapCsucs2_vec2(teglalapCsucs2x, teglalapCsucs2y);

float movablePoint1x = 300.0f;
float movablePoint1y = 300.0f;
vec2 movablePoint1_vec2(movablePoint1x, movablePoint1y);

float movablePoint2x = 350.0f;
float movablePoint2y = 350.0f;
vec2 movablePoint2_vec2(movablePoint2x, movablePoint2y);

// my beloved circle
vec2 speedAndDirectionOfCircle;
vec2 origoOfCircle;
GLdouble rayOfCircle;

std::vector<vec2> draggablePoints;
std::vector<vec2> computedPointsOfLine(2);
std::vector<vec2> coloredPoints;

bool doubleEquals(double first, double second, double treshold = 0.0001) {
	bool output = false;
	if (std::abs(first - second) <= treshold * std::abs(first)) {
		output = true;
	}
	return output;
}

void initCircle() {
	speedAndDirectionOfCircle.x = 1.0;
	speedAndDirectionOfCircle.y = 1.0;
	origoOfCircle.x = (float)(draggablePoints[0].x + (draggablePoints[1].x  * 0.50f ));
	origoOfCircle.y = (float)(draggablePoints[0].y + (draggablePoints[1].y  * 0.25f));
	rayOfCircle = 30.0;
}

void initColoredPoints() {
	for (int index1 = 0; index1 < winHeight; index1 += 25) {
		for (int indexInside = 0; indexInside < winWidth; indexInside += 25) {
			if ((index1 % 50) == 0) {
				indexInside += 12;
			}
			vec2 actualPoint;
			actualPoint.x = (float)indexInside;
			actualPoint.y = (float)index1;
			coloredPoints.push_back(actualPoint);
		}
	}
}

void initDraggablePoints(){
	// Az els� kett� vec2 pont lesz a t�glalap!
	draggablePoints.push_back(teglalapCsucs1_vec2);
	draggablePoints.push_back(teglalapCsucs2_vec2);

	// A 3. �s negyedik vec2 pont lesz az egyenes k�t
	draggablePoints.push_back(movablePoint1_vec2);
	draggablePoints.push_back(movablePoint2_vec2);
}

void drawCircle(vec2 O, GLdouble r) {

	glColor3ub(0,0,0);
	glBegin(GL_POINTS);
	GLdouble circleX;
	GLdouble circleY;
	for (GLdouble t = 0; t <= 2 * pi(); t += 0.01) {
		circleX = O.x + r * cos(t);
		circleY = O.y + r * sin(t);
		glVertex2d(circleX, circleY);
	}
	glEnd();
}

vec2 makeVectorFrom2Points(vec2 pointFrom, vec2 pointTo) {
	vec2 output = pointTo - pointFrom;
	return output;
}

vec2 makeNormalVec2(vec2 input) {
	vec2 output(input.y, -1.0f * input.x);
	return output;
}

float makeC1InLinesEquation(vec2 normalVector, vec2 p0) { // where P0 is one of the point of the Line
	float output = -1.0f * normalVector.x * p0.x - normalVector.y * p0.y;
	return output;
}

std::tuple<float, float, float> makeLinesEquation(vec2 normalVector, vec2 p0) {
	float a = normalVector.x;
	float b = normalVector.y;
	float c = makeC1InLinesEquation(normalVector, p0);
	
	/*if (b < -0.0f) {
		a *= -1.0f;
		b *= -1.0f;
		c *= -1.0f;
	}*/
	std::tuple<float, float, float> output = std::make_tuple(a, b, c);
	return output;
}

std::tuple<float, float, float> makeLinesEquationFrom2Points(vec2 point1, vec2 point2) {
	vec2 directionVector = makeVectorFrom2Points(point1, point2);
	vec2 normalVector = makeNormalVec2(directionVector);
	std::tuple<float, float, float> output =  makeLinesEquation(normalVector, point1);
	return output;
}

// The point is on the line when returned float is 0.0f
// if less ore more then the point is on one of the side of the line
bool decideWhichSideThePointIsFromTheLine(vec2 examinedPoint, const vec2 const &linePoint1, const vec2 const &linePoint2) {
	std::tuple<float, float, float> lineEquation = makeLinesEquationFrom2Points(linePoint1, linePoint2);
	float a = std::get<0>(lineEquation);
	float b = std::get<1>(lineEquation);
	float c = std::get<2>(lineEquation);
	bool output;
	/*if (b < 0) {
		a *= -1.0f;
		b *= -1.0f;
		c *= -1.0f;
	}*/
	float decider = a * examinedPoint.x + b * examinedPoint.y;
	if (decider < c * -1.0f) {
		output = false;
	}
	else
	{
		output = true;
	}

	if (doubleEquals(draggablePoints[0].x, draggablePoints[0].x)) {
		output = !output;
	}

	return output;
}

double distanceOfPointAndLine(vec2 examinedPoint, const vec2 const &linePoint1, const vec2 const &linePoint2) {
	std::tuple<float, float, float> lineEquation = makeLinesEquationFrom2Points(linePoint1, linePoint2);
	float a = std::get<0>(lineEquation);
	float b = std::get<1>(lineEquation);
	float c = std::get<2>(lineEquation);
	double output;
	/*if (b < 0) {
	a *= -1.0f;
	b *= -1.0f;
	c *= -1.0f;
	}*/
	output = std::abs(a * examinedPoint.x + b * examinedPoint.y + c) / std::sqrt(std::pow(a, 2.0) + std::pow(b, 2.0));

	return output;
}

std::vector<std::vector<float> > make2x2Matrix(float a00, float a01, float a10, float a11) {
	std::vector < std::vector<float> > STDVector2d(2);
	STDVector2d[0].push_back(a00);
	STDVector2d[0].push_back(a01);
	STDVector2d[1].push_back(a10);
	STDVector2d[1].push_back(a11);
	return STDVector2d;
}

float detOf2x2Matrix(std::vector<std::vector<float> > input) {
	// det(M) = ad - bc
	//  [ a  c ]
	//  [ b  d ]
	float output = (float)(input[0][0] * input[1][1] - input[1][0] * input[0][1]);
	return output;
}

vec2 getIntersectPoint(vec2 line1a, vec2 line1b, vec2 line2a, vec2 line2b) {
	vec2 irany1 = makeVectorFrom2Points(line1a, line1b);
	vec2 normal1 = makeNormalVec2(irany1);
	std::tuple<float, float, float> lineEquation1 = makeLinesEquation(normal1, line1a);
	float a1 = std::get<0>(lineEquation1);
	float b1 = std::get<1>(lineEquation1);
	float c1 = std::get<2>(lineEquation1);

	vec2 irany2 = makeVectorFrom2Points(line2a, line2b);

	vec2 normal2 = makeNormalVec2(irany2);
	std::tuple<float, float, float> lineEquation2 =  makeLinesEquation(normal2, line2a);
	float a2 = std::get<0>(lineEquation2);
	float b2 = std::get<1>(lineEquation2);
	float c2 = std::get<2>(lineEquation2);

	std::vector<std::vector<float> > xMatrixOsztando = make2x2Matrix(-1.0f * c1, b1, -1.0f * c2, b2);
	std::vector<std::vector<float> > yMatrixOsztando = make2x2Matrix(a1, -1.0f* c1, a2, -1.0f * c2);
	std::vector<std::vector<float> > xyMatrixOszto = make2x2Matrix(a1, b1, a2, b2);

	float detOfXMatrixOsztando = detOf2x2Matrix(xMatrixOsztando);
	float detOfYMatrixOsztando = detOf2x2Matrix(yMatrixOsztando);
	float detOfXYMatrixOszto = detOf2x2Matrix(xyMatrixOszto);

	vec2 output;
	output.x = detOfXMatrixOsztando / detOfXYMatrixOszto;
	output.y = detOfYMatrixOsztando / detOfXYMatrixOszto;

	return output;
}

void writeOutDraggablePoints() {
	for (vec2 point : draggablePoints) {
		printMathObject( point );
	}
}

void init()
{
	glClearColor(1.0, 1.0, 1.0, 0.0);
	glMatrixMode(GL_PROJECTION);
	gluOrtho2D(0.0, winWidth, 0.0, winHeight);
	glShadeModel(GL_FLAT);
	glEnable(GL_POINT_SMOOTH);
	glPointSize(10.0);
	glLineWidth(5.0);

	initColoredPoints();
	initDraggablePoints();
	writeOutDraggablePoints();
	initCircle();
}

void drawOuterRectangle() {
	glColor3ub (0, 0, 0);
	glBegin(GL_LINE_LOOP);
		glVertex2f(draggablePoints[0].x, draggablePoints[0].y);
		glVertex2f(draggablePoints[1].x, draggablePoints[0].y);
		glVertex2f(draggablePoints[1].x, draggablePoints[1].y);
		glVertex2f(draggablePoints[0].x, draggablePoints[1].y);
	glEnd();
}

void drawDraggableRectanglePoints() {
	glColor3ub(255, 0, 0);
	glBegin(GL_POINTS);
		glVertex2f(draggablePoints[0].x, draggablePoints[0].y);
		glVertex2f(draggablePoints[1].x, draggablePoints[1].y);
	glEnd();
}

void drawDraggableLinePoints() {
	glColor3ub(0, 0, 255);
	glBegin(GL_POINTS);
		glVertex2f(draggablePoints[2].x, draggablePoints[2].y);
		glVertex2f(draggablePoints[3].x, draggablePoints[3].y);
	glEnd();
}

bool isPointOnTheLine(vec2 theExaminedPoint, vec2 linePoint1, vec2 linePoint2) {
	bool output = false;
	if( (((std::min(linePoint1.x, linePoint2.x) < theExaminedPoint.x) || 
		floatEqual(std::min(linePoint1.x, linePoint2.x), theExaminedPoint.x)) 
			&&
		((std::max(linePoint1.x, linePoint2.x) > theExaminedPoint.x) ||
		floatEqual(std::max(linePoint1.x, linePoint2.x), theExaminedPoint.x))) 
				&&
		(((std::min(linePoint1.y, linePoint2.y) < theExaminedPoint.y) ||
		floatEqual(std::min(linePoint1.y, linePoint2.y), theExaminedPoint.y))
			&&
		((std::max(linePoint1.y, linePoint2.y) > theExaminedPoint.y) ||
		floatEqual(std::max(linePoint1.y, linePoint2.y), theExaminedPoint.y)))	
	)	{
		output = true;
	}
	return output;
}

void drawdrawUnDraggableLinePointsHelper(vec2 pointCoordinates) {
	glColor3ub(238, 66, 244);
	
	glBegin(GL_POINTS);
		glPointSize(10.0);
		glVertex2f(pointCoordinates.x, pointCoordinates.y);
	glEnd();
}

void countUnDraggableLinePoints() {
	vec2 rectLeftDown = draggablePoints[0];
	vec2 rectRightDown(draggablePoints[1].x, draggablePoints[0].y);
	vec2 rectRightUp = draggablePoints[1];
	vec2 rectLeftUp(draggablePoints[0].x, draggablePoints[1].y);

	vec2 possiblyOnDown = getIntersectPoint(draggablePoints[2], draggablePoints[3], rectLeftDown, rectRightDown);
	vec2 possiblyOnRight = getIntersectPoint(draggablePoints[2], draggablePoints[3], rectRightDown, rectRightUp);
	vec2 possiblyOnUp = getIntersectPoint(draggablePoints[2], draggablePoints[3], rectLeftUp, rectRightUp);
	vec2 possiblyOnLeft = getIntersectPoint(draggablePoints[2], draggablePoints[3], rectLeftDown, rectLeftUp);

	bool vanBal = isPointOnTheLine(possiblyOnLeft, rectLeftDown, rectLeftUp);
	bool vanLent = isPointOnTheLine(possiblyOnDown, rectLeftDown, rectRightDown);
	bool vanFent = isPointOnTheLine(possiblyOnUp, rectLeftUp, rectRightUp);
	bool vanJobb = isPointOnTheLine(possiblyOnRight, rectRightUp, rectRightDown);


	if (vanLent) {
		computedPointsOfLine[0] = possiblyOnDown;
		if (vanBal) {
			computedPointsOfLine[1] = possiblyOnLeft;
		}
		if (vanFent) {
			computedPointsOfLine[1] = possiblyOnUp;
		}
		if (vanJobb) {
			computedPointsOfLine[1] = possiblyOnRight;
		}
	}

	if (vanBal) {
		computedPointsOfLine[0] = possiblyOnLeft;
		if (vanLent) {
			computedPointsOfLine[1] = possiblyOnDown;
		}
		if (vanFent) {
			computedPointsOfLine[1] = possiblyOnUp;
		}
		if (vanJobb) {
			computedPointsOfLine[1] = possiblyOnRight;
		}
	}

	if (vanFent) {
		computedPointsOfLine[0] = possiblyOnUp;
		if (vanLent) {
			computedPointsOfLine[1] = possiblyOnDown;
		}
		if (vanBal) {
			computedPointsOfLine[1] = possiblyOnLeft;
		}
		if (vanJobb) {
			computedPointsOfLine[1] = possiblyOnRight;
		}
	}

	if (vanJobb) {
		computedPointsOfLine[0] = possiblyOnRight;
		if (vanLent) {
			computedPointsOfLine[1] = possiblyOnDown;
		}
		if (vanBal) {
			computedPointsOfLine[1] = possiblyOnLeft;
		}
		if (vanFent) {
			computedPointsOfLine[1] = possiblyOnUp;
		}
	}

	if (vanLent &&vanFent) {
		computedPointsOfLine[0] = possiblyOnDown;
		computedPointsOfLine[1] = possiblyOnUp;
	}
	if (vanBal &&vanJobb) {
		computedPointsOfLine[0] = possiblyOnLeft;
		computedPointsOfLine[1] = possiblyOnRight;
	}
	if (vanJobb && vanFent) {
		computedPointsOfLine[0] = possiblyOnUp;
		computedPointsOfLine[1] = possiblyOnRight;
	}
	if (vanJobb && vanLent) {
		computedPointsOfLine[0] = possiblyOnDown;
		computedPointsOfLine[1] = possiblyOnRight;
	}
	if (vanBal &&vanFent)	{
		computedPointsOfLine[0] = possiblyOnLeft;
		computedPointsOfLine[1] = possiblyOnUp;
	}
	/*if (vanBal &&vanLent) {
		if (vanFent || false) {
			computedPointsOfLine[0] = possiblyOnLeft;
			computedPointsOfLine[1] = possiblyOnDown;
		}
		
	}*/

	if (vanJobb && vanBal && vanFent && vanLent) {
		computedPointsOfLine[0] = possiblyOnLeft;
		computedPointsOfLine[1] = possiblyOnRight;
	}
}

void drawTheLine() {
	glColor3ub(64, 64, 64);

	glBegin(GL_LINES);
		glPointSize(10.0);
		glVertex2f(computedPointsOfLine[0].x, computedPointsOfLine[0].y);
		glVertex2f(computedPointsOfLine[1].x, computedPointsOfLine[1].y);
	glEnd();
}

void drawUnDraggableLinePoints() {

	countUnDraggableLinePoints();
	drawdrawUnDraggableLinePointsHelper(computedPointsOfLine[0]);
	drawdrawUnDraggableLinePointsHelper(computedPointsOfLine[1]);

	glColor3ub(64, 64, 64);
	
	glBegin(GL_LINES);
		glPointSize(10.0);
		glVertex2f(computedPointsOfLine[0].x, computedPointsOfLine[0].y);
		glVertex2f(computedPointsOfLine[1].x, computedPointsOfLine[1].y);
	glEnd();

}

bool isPointInsideTheCircle(vec2 thePoint, vec2 theCirclesOrigo, double theRayOfCircle) {
	return (pow((thePoint.x - theCirclesOrigo.x), 2.0) + pow((thePoint.y - theCirclesOrigo.y), 2.0)) - pow(theRayOfCircle, 2.0) < 0.0;
}

void drawColoredPoints() {
	vec2 teglalapPont1 = draggablePoints[0];
	vec2 teglalapPont2 = draggablePoints[1];
	
	glBegin(GL_POINTS);
		glPointSize(5.0);
		for (vec2 datPoint : coloredPoints) {
			if (std::min(teglalapPont1.x, teglalapPont2.x) <= datPoint.x && 
				std::max(teglalapPont1.x, teglalapPont2.x) >= datPoint.x &&
				std::min(teglalapPont1.y, teglalapPont2.y) <= datPoint.y &&
				std::max(teglalapPont1.y, teglalapPont2.y) >= datPoint.y 
			) {
				if (isPointInsideTheCircle(datPoint, origoOfCircle, rayOfCircle)) {
					glColor3ub(255, 255, 0);
					glVertex2f(datPoint.x, datPoint.y);;
				}
				else
				{
					if (decideWhichSideThePointIsFromTheLine(datPoint, draggablePoints[2], draggablePoints[3])) {
						glColor3ub(100, 255, 100);
						glVertex2f(datPoint.x, datPoint.y);
					}
					else
					{
						glColor3ub(255, 100, 100);
						glVertex2f(datPoint.x, datPoint.y);
					}
				}
			}
		}
	glEnd();
}


void display()
{
	glClear(GL_COLOR_BUFFER_BIT);

	drawOuterRectangle();
	glPointSize(10.0);
	drawDraggableRectanglePoints();
	glPointSize(10.0);
	drawDraggableLinePoints();
	glPointSize(10.0);
	drawUnDraggableLinePoints();
	glPointSize(5.0);
	drawColoredPoints();
	drawCircle(origoOfCircle, rayOfCircle);
	drawTheLine();

	glutSwapBuffers();
}

GLint getActivePoint3(const std::vector<vec2> const &draggablePoints, GLint sens, GLint x, GLint y)
{
	GLint index, sensPower2 = sens * sens;
	vec2 clickedPoint_vec2 = { (float)x, (float)y };

	GLint size = static_cast<GLint>(draggablePoints.size());

	for (index = 0; index < size; index++)
		if (dist2(draggablePoints.at(index), clickedPoint_vec2) < sensPower2)
			return index;
	return -1;
}

void processMouse(GLint button, GLint action, GLint xMouse, GLint yMouse)
{
	GLint i;
	if (button == GLUT_LEFT_BUTTON && action == GLUT_DOWN)
		if ((i = getActivePoint3(draggablePoints, pointSensitivity, xMouse, winHeight - yMouse)) != -1)
			dragged = i;
	if (button == GLUT_LEFT_BUTTON && action == GLUT_UP)
		dragged = -1;
}

void processMouseActiveMotion(GLint xMouse, GLint yMouse)
{
	if (dragged >= 0) {
		draggablePoints[dragged].x = (float)xMouse;
		draggablePoints[dragged].y = (float)(winHeight - yMouse);
		glutPostRedisplay();
	}
}

void update(int n) {
	origoOfCircle.x = origoOfCircle.x + speedAndDirectionOfCircle.x;
	origoOfCircle.y = origoOfCircle.y + speedAndDirectionOfCircle.y;

	if ((origoOfCircle.x + rayOfCircle) > draggablePoints[1].x) {
		speedAndDirectionOfCircle.x = (-1) * std::abs(speedAndDirectionOfCircle.x);
	}

	if ((origoOfCircle.x - rayOfCircle) < draggablePoints[0].x) {
		speedAndDirectionOfCircle.x = std::abs(speedAndDirectionOfCircle.x);
	}

	if ((origoOfCircle.y + rayOfCircle) > draggablePoints[1].y) {
		speedAndDirectionOfCircle.y = (-1) * std::abs(speedAndDirectionOfCircle.y);
	}

	if ((origoOfCircle.y - rayOfCircle) < draggablePoints[0].y) {
		speedAndDirectionOfCircle.y = std::abs(speedAndDirectionOfCircle.y);
	}

	double distanceOfOrigoOfCircle = distanceOfPointAndLine(origoOfCircle, draggablePoints[2], draggablePoints[3]);
	if ( rayOfCircle > distanceOfOrigoOfCircle) {
		vec2 directionVec = makeVectorFrom2Points(draggablePoints[2], draggablePoints[3]);
		vec2 normalVectorA = makeNormalVec2(directionVec);
		vec2 reflectedDirection = ( -1.0f * speedAndDirectionOfCircle) + 2 *
			((dot(normalVectorA, speedAndDirectionOfCircle)) / (pow(length(normalVectorA), 2.0f)))  * normalVectorA;
		//origoOfCircle += reflectedDirection;
		std::cout << "Direction changed from: " << speedAndDirectionOfCircle.x << ", " << speedAndDirectionOfCircle.y << "\n";
		std::cout << "Direction changed to  : " << reflectedDirection.x << ", " << reflectedDirection.y << "\n";


		speedAndDirectionOfCircle = -1.0f *reflectedDirection;
		origoOfCircle += speedAndDirectionOfCircle;
	}

	glutPostRedisplay();

	glutTimerFunc(5, update, 0);
}

int main(int argc, char** argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
	glutInitWindowSize(winWidth, winHeight);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("2. Beadando: Daradics Levente");
	init();
	glutDisplayFunc(display);
	glutMouseFunc(processMouse);
	glutMotionFunc(processMouseActiveMotion);

	glutTimerFunc(5, update, 0);

	glutMainLoop();
	return 0;
}


